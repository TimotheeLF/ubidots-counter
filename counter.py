import requests
import platform
import RPi.GPIO as GPIO
import time
import threading

GPIO.setmode(GPIO.BCM)
GPIO.setup(7, GPIO.IN)
# Voir https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-reduced-schematics.pdf
# pour le schéma des GPIOs et donc savoir ou se connecter.

DEVICE_LABEL = "Boites"
VARIABLE_LABEL = "Counter"
UBIDOTS_TOKEN = "" # Ajouter le token Ubidots.

# Utilise des threads pour parler au serveur en parallèle.
def send_data_to_ubidots(ubidots_token, device_label, variable_label, value):
    try:
        kwargs = {
            "headers": {"X-Auth-Token": ubidots_token, "Content-type": "application/json"},
            "json": {variable_label: value},
            "url": f"https://industrial.api.ubidots.com/api/v1.6/devices/{device_label}",
            "method": "post"
        }

        print(f"Résultats de la requête:\n", requests.request(**kwargs).text + "\n")
        print("201 -> OK")

    except Exception as e:
        print(f"Il y a une erreur en essayant de contacter le serveur:\n{e}")
    
    return None


def main():
    counter = 0
    todo = 0        # todo: 1 -> Ne détecte pas la présence car il y avait déjà une boîte devant.
    send = 0        # send: 0 -> Permet d'envoyer au serveur. Cela évite de ping le serveur quand il n'y a pas de boîtes.
    start = time.time()
    while True:
        # Détecte si le capteur est actionné par quelque chose.
        presence = GPIO.input(7)
        if (presence):
            if (todo == 0):
                print("Boite détectée\n")
                send = 0
                todo = 1
                counter += 1
                time.sleep(0.3)
        else:
            todo = 0
        
        # À chaque 5 secondes, envoyer au serveur les données.
        # 20 secondes sont parfaites pour respecter la limite de 4000 requêtes par jour pour un compte gratuit.
        if (time.time() - start >= 5 and send == 0):
            print(f"Valeur du compteur: {counter}, tentative de contact avec le serveur ...")
            thread1 = threading.Thread(target = send_data_to_ubidots, args = (UBIDOTS_TOKEN, DEVICE_LABEL,
                                   VARIABLE_LABEL, counter)

            thread1.start()

            start = time.time()
            send = 1

        # Dans le cas d'un ajout de bouton pour remettre à zéro
        # Quand on clique, le remettre à zero et contacter le serveur
        # TODO: Modifier le input pour le chiffre correct et ajouter au début du programme le setup.
        # click = GPIO.input(8)
        # if (click):
            # counter = 0
            # send = 0


if __name__ == "__main__":
    python_version = platform.python_version_tuple()
    if int(python_version[0]) < 3 or int(python_version[0]) >= 3 and int(python_version[1]) < 5:
        print("please upgrade your python version to python 3.6, cancelling routine")
    else:
        main()

